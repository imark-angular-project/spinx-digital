import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { AppServiceLayoutComponent } from './layout/ourservicessingle/our-service-layout';
import { AppBlogSingleLayoutComponent } from './layout/blogsingle/blogsingle-layout';
import { AppPortfolioSingleLayoutComponent } from './layout/portfoliosingle/portfolio-layout';
import { HomeComponent } from './layout/home/home.component';
import { OurservicesComponent } from './layout/ourservices/ourservices.component';
import { WebDevelopmentComponent } from './layout/ourservicessingle/webDevelopment/webDevelopment.component';
import { AndroidComponent } from './layout/ourservicessingle/android/android.component';
import { AppPrototypingComponent } from './layout/ourservicessingle/appPrototyping/appPrototyping.component';
import { UserExperienceComponent } from './layout/ourservicessingle/user-experience/user-experience.component';
import { IphoneComponent } from './layout/ourservicessingle/iphone/iphone.component';
import { MobileAppComponent } from './layout/ourservicessingle/mobileApp/mobileApp.component';
import { AboutComponent } from './layout/about/about.component';
import { PortfolioComponent } from './layout/portfolio/portfolio.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfolio1/portfoliosingle.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BlogsingleComponent } from './layout/blogsingle/blog1/blogsingle.component';
import { Blogsingle2Component } from './layout/blogsingle/blog2/blogsingle.component';
import { Blogsingle3Component } from './layout/blogsingle/blog3/blogsingle.component';
import { ContactusComponent } from './layout/contactus/contactus.component';
import { ThankYouComponent } from './layout/thank-you/thankyou.component';

export const routes: Routes = [
    {
        path: '',
        component: AppLayoutComponent,
        children: [
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'services',  component: OurservicesComponent },
            { path: 'about', component: AboutComponent },
            { path: 'blog', component: BlogComponent },
            { path: 'portfolio', component: PortfolioComponent },
            { path: 'contact-us', component: ContactusComponent },
            { path: 'thank-you', component: ThankYouComponent }
        ]
    },
    {
        path: 'services',
        component: AppServiceLayoutComponent,
        children: [
            { path: 'web-development', component: WebDevelopmentComponent },
            { path: 'android', component: AndroidComponent },
            { path: 'app-prototyping', component: AppPrototypingComponent },
            { path: 'user-experience', component: UserExperienceComponent },
            { path: 'iphone', component: IphoneComponent },
            { path: 'mobile-app-development', component: MobileAppComponent }
        ]
    },
    {
        path: 'blog',
        component: AppBlogSingleLayoutComponent,
        children: [
            { path: 'understanding-web-native-and-hybrid-mobile-applications', component: BlogsingleComponent },
            { path: 'web-design-trends-to-look-out-for-in-2019', component: Blogsingle2Component },
            { path: '5-compelling-reasons-for-embracing-ways-to-boost-your-sales', component: Blogsingle3Component }
        ]
    },
    {
        path: 'portfolio',
        component: AppPortfolioSingleLayoutComponent,
        children: [
            { path: 'portfolio-inner', component: PortfoliosingleComponent }
        ]
    }

];
@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
  })
  export class AppRoutingModule {}