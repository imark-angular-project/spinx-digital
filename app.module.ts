import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { AppServiceLayoutComponent } from './layout/ourservicessingle/our-service-layout';
import { AppBlogSingleLayoutComponent } from './layout/blogsingle/blogsingle-layout';
import { AppPortfolioSingleLayoutComponent } from './layout/portfoliosingle/portfolio-layout';
import { AppHeaderComponent } from './layout/app-header/app-header-component';
import { HomeComponent } from './layout/home/home.component';
import { OurservicesComponent } from './layout/ourservices/ourservices.component';
import { WebDevelopmentComponent } from './layout/ourservicessingle/webDevelopment/webDevelopment.component';
import { AndroidComponent } from './layout/ourservicessingle/android/android.component';
import { AppPrototypingComponent } from './layout/ourservicessingle/appPrototyping/appPrototyping.component';
import { UserExperienceComponent } from './layout/ourservicessingle/user-experience/user-experience.component';
import { IphoneComponent } from './layout/ourservicessingle/iphone/iphone.component';
import { MobileAppComponent } from './layout/ourservicessingle/mobileApp/mobileApp.component';
import { AboutComponent } from './layout/about/about.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BlogsingleComponent } from './layout/blogsingle/blog1/blogsingle.component';
import { Blogsingle2Component } from './layout/blogsingle/blog2/blogsingle.component';
import { Blogsingle3Component } from './layout/blogsingle/blog3/blogsingle.component';
import { PortfolioComponent } from './layout/portfolio/portfolio.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfolio1/portfoliosingle.component';
import { ContactusComponent } from './layout/contactus/contactus.component';
import { AppFooterComponent } from './layout/app-footer/app-footer-component';
import { ThankYouComponent } from './layout/thank-you/thankyou.component';

import { AppRoutingModule } from './app.routing';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, AppRoutingModule, BrowserAnimationsModule ],
  declarations: [ AppComponent, AppHeaderComponent, AppLayoutComponent, HomeComponent, OurservicesComponent, AboutComponent, BlogComponent,  AppFooterComponent, PortfolioComponent,PortfoliosingleComponent, ContactusComponent, AppServiceLayoutComponent, WebDevelopmentComponent, AndroidComponent, AppPrototypingComponent, UserExperienceComponent, IphoneComponent, MobileAppComponent, AppBlogSingleLayoutComponent, BlogsingleComponent, Blogsingle2Component, Blogsingle3Component, AppPortfolioSingleLayoutComponent, ThankYouComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
